﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace ExampleProject_Tetris.Visuals.Model
{
    /// <summary>
    /// The data of a shape. 
    /// </summary>
    class Shape
    {
        /// <summary>
        /// The grid points the shape is on. 
        /// </summary>
        public Point[] Parts { get; set; }

        /// <summary>
        /// The height of the shape, in cells.
        /// </summary>
        public int Height { get; private set; }

        /// <summary>
        /// The width of the shape, in cells. 
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// Center of rotation (will be equal to one of the points of the shape).
        /// </summary>
        public Vector CenterOfRotation { get; set; }

        /// <summary>
        /// Shows if rotation is allowed for this shape.
        /// </summary>
        public bool AllowsRotation { get; set; }

        private Shape(Point[] parts, int height, int width, int centerOfRotationPartIndex = -1)
        {
            bool hasCenterOfRotation = centerOfRotationPartIndex != -1;
            
            Point centerOfRotation = 
                hasCenterOfRotation ? 
                   parts[centerOfRotationPartIndex] : new Point(0, 0);

            this.Parts = new Point[parts.Length];

            // Normalizing every point of the shape to be relative to the 
            // center of rotation.
            // CenterOfRotation can then be 0,0 at the start of the game. 
            for (int i = 0; i < parts.Length; i++)
            {
                this.Parts[i] = new Point(parts[i].X - centerOfRotation.X, parts[i].Y - centerOfRotation.Y);
            }

            AllowsRotation = hasCenterOfRotation;
            CenterOfRotation = new Vector(0, 0);
            Height = height;
            Width = width;
        }

        private Shape(Shape shape)
        {
            Parts = new Point[shape.Parts.Length];
            for (int i = 0; i < Parts.Length; i++)
            {
                Parts[i] = shape.Parts[i];
            }

            CenterOfRotation = shape.CenterOfRotation;
            AllowsRotation = shape.AllowsRotation;
            Height = shape.Height;
            Width = shape.Width;
        }

        /// <summary>
        /// Copies a shape. This is one of the ways a shape can be created.
        /// </summary>
        /// <returns>The copied shape.</returns>
        public Shape Copy()
        {
            return new Shape(this);
        }

        /// <summary>
        /// Parses a shape from a string.
        /// </summary>
        /// <param name="shapeString">Can contain any characters, 
        /// but only 'o's and 'x'es will count as useful parts (=points) of a shape. 
        /// 'o' = the center of rotation of the object
        /// 'x' = a point of the object</param>
        /// <returns>The shape that has been created.</returns>
        public static Shape Parse(string shapeString)
        {
            string[] lines = shapeString.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            int centerOfRotationPartIndex = -1;
            List<Point> parts = new List<Point>();

            int shapeHeight = lines.Length;
            int shapeWidth = int.MinValue;

            for (int y = 0; y < shapeHeight; y++)
            {
                if (lines[y].Length > shapeWidth)
                {
                    shapeWidth = lines[y].Length;
                }

                for (int x = 0; x < lines[y].Length; x++)
                {
                    switch (lines[y][x])
                    {
                        case 'o':
                            centerOfRotationPartIndex = parts.Count;
                            parts.Add(new Point(x, y));
                            break;
                        case 'x':
                            parts.Add(new Point(x, y));
                            break;
                    }
                }
            }

            return new Shape(parts.ToArray(), shapeHeight, shapeWidth, centerOfRotationPartIndex);
        }
    }
}
