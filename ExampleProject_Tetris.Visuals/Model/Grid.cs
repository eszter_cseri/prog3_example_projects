﻿using System.Windows;

namespace ExampleProject_Tetris.Visuals.Model
{
    /// <summary>
    /// The data of a grid. 
    /// </summary>
    class Grid
    {
        /// <summary>
        /// Default width of the grid.
        /// </summary>
        public const int defaultWidth = 11;
        
        /// <summary>
        /// Default height of the grid. 
        /// </summary>
        public const int defaultHeight = 16;

        /// <summary>
        /// Cells in the grid. False means the cell is free.
        /// </summary>
        public bool[,] Cells { get; private set; }
        
        /// <summary>
        /// Width of the grid. 
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// Height of the grid.
        /// </summary>
        public int Height { get; private set; }

        /// <summary>
        /// The point the shapes will appear on.
        /// </summary>
        public Vector StartPoint { get; private set; }

        /// <summary>
        /// Creates a grid.
        /// </summary>
        /// <param name="width">Width, optional, defaults to the defaultwidth.</param>
        /// <param name="height">Height, optional, defaults to the defaultheight.</param>
        public Grid(int width = defaultWidth, int height = defaultHeight)
        {
            Height = height;
            Width = width;
            Cells = new bool[width, height];
            StartPoint = new Vector(width / 2, 0);
        }
    }
}
