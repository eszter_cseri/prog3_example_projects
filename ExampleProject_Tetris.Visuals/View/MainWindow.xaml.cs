﻿using System.Windows;

using ExampleProject_Tetris.Visuals.BL;

namespace ExampleProject_Tetris.Visuals
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            gameFrameworkElement.Game = new Game();
            gameFrameworkElement.Init();
        }
    }
}
