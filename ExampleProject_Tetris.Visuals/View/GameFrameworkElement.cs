﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using ExampleProject_Tetris.Visuals.BL;

namespace ExampleProject_Tetris.Visuals.View
{
    /// <summary>
    /// Visualizes the game and handles timing and user interaction.
    /// </summary>
    class GameFrameworkElement : FrameworkElement
    {
        private const int smallCellWidth = 10;
        private const int smallCellHeight = 10;
        private readonly Pen shapePen = new Pen(Brushes.Red, 1);
        private readonly Pen gridPen = new Pen(Brushes.Blue, 1);

        private DispatcherTimer timer;

        private ImageBrush arrow;

        private Game game;

        /// <summary>
        /// The game. 
        /// </summary>
        public Game Game
        {
            get { return game; }
            set { game = value; InvalidateVisual(); }
        }

        public GameFrameworkElement()
        {
            this.Loaded += GameFrameworkElement_Loaded;
            this.KeyDown += GameFrameworkElement_KeyDown;
        }

        /// <summary>
        /// Initializes the game, loads all the necessary resources, sets the game to starting state.
        /// </summary>
        public void Init()
        {
            if (Game == null)
                return;

            Game.Init();

            // Preloading of image resource (that was added as content) to avoid loading every time we draw.
            // More info on resources: http://stackoverflow.com/questions/145752/what-are-the-various-build-action-settings-in-visual-studio-project-properties 
            arrow = new ImageBrush(new BitmapImage(new Uri("Pics\\rotate_48.png", UriKind.Relative)));

            InvalidateVisual();
        }

        private void GameFrameworkElement_Loaded(object sender, RoutedEventArgs e)
        {
            this.Focusable = true;
            this.Focus();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (Game != null)
            {
                Game.DoTurn();
                if (Game.GameOver)
                {
                    timer.Stop();
                }

                this.InvalidateVisual();
            }
        }

        private void GameFrameworkElement_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Space || e.Key == System.Windows.Input.Key.Up)
            {
                Game.Rotate();
            }
            else if (e.Key == System.Windows.Input.Key.Down)
            {
                Game.Move(0, 1);
            }
            else if (e.Key == System.Windows.Input.Key.Left)
            {
                Game.Move(-1, 0);
            }
            else if (e.Key == System.Windows.Input.Key.Right)
            {
                Game.Move(1, 0);
            }
            else if (e.Key == System.Windows.Input.Key.F)
            {
                Game.DoTurn();
            }

            InvalidateVisual();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (Game == null || !Game.Initialized)
                return;

            double cellWidth = this.ActualWidth / Game.Grid.Width;
            double cellHeight = this.ActualHeight / Game.Grid.Height;

            // Drawing black background
            drawingContext.DrawRectangle(Brushes.Black, null,
                new Rect(0, 0, this.ActualWidth, this.ActualHeight));

            // Drawing grid
            DrawGrid(drawingContext, cellWidth, cellHeight);

            // Drawing current shape 
            DrawCurrentShape(drawingContext, cellWidth, cellHeight);

            // Drawing score multiplier if > 1  
            DrawMultiplierText(drawingContext);

            // Drawing next shape, always a 'smallcellwidth' and 'smallcellheight' distance apart 
            // from the topright corner of the game screen. 
            DrawNextShape(drawingContext);

            // drawing scores last, so it is always on top. 
            // Also 'smallcellwidth' and 'smallcellheight' apart from topleft 
            DrawScores(drawingContext);

            // drawing game over text. 
            DrawGameOverText(drawingContext);
        }

        private void DrawGameOverText(DrawingContext drawingContext)
        {
            if (Game.GameOver)
            {
                FormattedText gameOverText = new FormattedText(
                    "Game over :(",
                    CultureInfo.CurrentUICulture,
                    FlowDirection.LeftToRight,
                    new Typeface("Verdana"),
                    32,
                    Brushes.Red);

                drawingContext.DrawText(gameOverText,
                    new Point(
                        this.ActualWidth / 2 - gameOverText.Width / 2,
                        this.ActualHeight / 2 - gameOverText.Height / 2));
            }
        }

        private void DrawScores(DrawingContext drawingContext)
        {
            FormattedText formattedText = new FormattedText(
                game.Score.ToString() + " ← your score!",
                CultureInfo.CurrentUICulture,
                FlowDirection.LeftToRight,
                new Typeface("Verdana"),
                16,
                game.Multiplier > 1 ? Brushes.Red : Brushes.Blue);

            drawingContext.DrawText(formattedText, new Point(smallCellWidth, smallCellHeight));
        }

        private void DrawNextShape(DrawingContext drawingContext)
        {
            int shapeDrawingWidth = Game.NextShape.Width * smallCellWidth;

            int minX = int.MaxValue;
            int minY = int.MaxValue;
            foreach (Point point in Game.NextShape.Parts)
            {
                if (point.X < minX)
                    minX = (int)point.X;

                if (point.Y < minY)
                    minY = (int)point.Y;
            }

            // nextshape is normalized so that the center of rotation is in 0,0, so we have to 
            // do some calculations to draw so that the drawing is always on the designated place.
            double shapeLeft = this.ActualWidth - shapeDrawingWidth - smallCellWidth;
            double shapeTop = smallCellHeight;
            foreach (Point point in Game.NextShape.Parts)
            {
                drawingContext.DrawRectangle(Brushes.DarkRed, shapePen,
                    new Rect(shapeLeft + (point.X - minX) * smallCellWidth,
                             shapeTop + (point.Y - minY) * smallCellHeight, smallCellWidth, smallCellHeight));
            }
        }

        private void DrawMultiplierText(DrawingContext drawingContext)
        {
            // Drawing multiplier text. 
            if (game.Multiplier > 1)
            {
                string text = game.Multiplier + "x" +
                    Environment.NewLine + "points for each line!!";
                FormattedText multiplierText = new FormattedText(
                    text,
                    CultureInfo.CurrentUICulture,
                    FlowDirection.LeftToRight,
                    new Typeface("Verdana"),
                    32,
                    Brushes.Red);
                multiplierText.TextAlignment = TextAlignment.Center;

                drawingContext.DrawText(multiplierText,
                    new Point(
                        this.ActualWidth / 2,
                        this.ActualHeight / 2 - multiplierText.Height / 2));
            }
        }

        private void DrawCurrentShape(DrawingContext drawingContext, double cellWidth, double cellHeight)
        {
            if (game.CurrentShapeActive)
            {
                foreach (Point point in Game.CurrentShape.Parts)
                {
                    drawingContext.DrawRectangle(Brushes.DarkRed, shapePen,
                        new Rect(point.X * cellWidth, point.Y * cellHeight, cellWidth, cellHeight));
                }

                if (Game.CurrentShape.AllowsRotation)
                {
                    // could be also drawing with DrawImage() here. 
                    drawingContext.DrawRectangle(arrow, null,
                            new Rect(
                            Game.CurrentShape.CenterOfRotation.X * cellWidth,
                            Game.CurrentShape.CenterOfRotation.Y * cellHeight,
                            cellWidth, cellHeight));
                }
            }
        }

        private void DrawGrid(DrawingContext drawingContext, double cellWidth, double cellHeight)
        {
            for (int x = 0; x < Game.Grid.Width; x++)
            {
                for (int y = 0; y < Game.Grid.Height; y++)
                {
                    if (Game.Grid.Cells[x, y])
                    {
                        drawingContext.DrawRectangle(Brushes.DarkBlue, gridPen,
                            new Rect(x * cellWidth, y * cellHeight, cellWidth, cellHeight));
                    }
                }
            }
        }
    }
}
