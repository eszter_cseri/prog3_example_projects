﻿using System;

namespace ExampleProject_Tetris.Visuals.BL
{
    /// <summary>
    /// Class for returning the goodness of the shape merge as event parameter.
    /// </summary>
    class MergeEventArgs : EventArgs
    {
        /// <summary>
        /// Goodness of the merge (=full grid lines created)
        /// </summary>
        public int MergeGoodness { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public MergeEventArgs(int mergeGoodness)
        {
            MergeGoodness = mergeGoodness;
        }
    }
}
