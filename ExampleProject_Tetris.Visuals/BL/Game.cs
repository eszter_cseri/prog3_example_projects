﻿using System;
using System.Windows;

using ExampleProject_Tetris.Visuals.Model;

namespace ExampleProject_Tetris.Visuals.BL
{
    /// <summary>
    /// Contains the main game logic.
    /// </summary>
    class Game
    {
        /// <summary>
        /// Returns the grid the game is played on. 
        /// </summary>
        public Grid Grid { get { return GridManager.Grid; } }

        /// <summary>
        /// Returns the shape that is currently falling.
        /// </summary>
        public Shape CurrentShape { get { return ShapeManager.CurrentShape; } }

        /// <summary>
        /// Returns whether the player is currently able to move the current shape.
        /// </summary>
        public bool CurrentShapeActive { get { return !GridManager.FallInProgress && !GameOver; } }

        /// <summary>
        /// A list of the various shapes that can appear in the game. 
        /// </summary>
        public ShapeList Shapes { get; private set; }

        /// <summary>
        /// Handles grid functions. 
        /// </summary>
        public GridManager GridManager { get; private set; }

        /// <summary>
        /// Handles the functions of the current shape. 
        /// </summary>
        public ShapeManager ShapeManager { get; private set; }

        /// <summary>
        /// The shape that will fall after the current one. 
        /// </summary>
        public Shape NextShape { get; private set; }

        /// <summary>
        /// Handles scoring 
        /// </summary>
        public Scores Scores { get; private set; }

        /// <summary>
        /// Current scores of player 
        /// </summary>
        public int Score { get { return Scores.Score; } }

        /// <summary>
        /// Multiplier on the score for each full line 
        /// </summary>
        public int Multiplier { get { return Scores.MergeMultiplier; } }

        /// <summary>
        /// Shows whether the game is still going.
        /// </summary>
        public bool GameOver { get; private set; }

        /// <summary>
        /// Shows whether the game is ready to start. 
        /// </summary>
        public bool Initialized { get; set; }

        /// <summary>
        /// Sets up the game before start.
        /// </summary>
        public void Init()
        {
            Scores = new Scores();
            GridManager = new GridManager(new Grid());
            ShapeManager = new ShapeManager(GridManager);

            Shapes = new ShapeList();
            NextShape = Shapes.GetRandom();
            SwitchShapes();

            Initialized = true;
            GameOver = false;

            ShapeManager.CantSwitchToShape += ShapeManager_CantSwitchToShape;
            ShapeManager.ShapeMerged += ShapeManager_ShapeMerged;
            GridManager.GridFallStarted += GridManager_GridFallStarted;
            GridManager.GridFallStopped += GridManager_GridFallStopped;
        }

        private void ShapeManager_ShapeMerged(object sender, EventArgs e)
        {
            SwitchShapes();
        }

        private void ShapeManager_CantSwitchToShape(object sender, EventArgs e)
        {
            GameOver = true;
        }

        private void GridManager_GridFallStarted(object sender, MergeEventArgs e)
        {
            Scores.SetMultiplierForMerge(e.MergeGoodness);
        }

        private void GridManager_GridFallStopped(object sender, EventArgs e)
        {
            Scores.ResetMultiplierForMerge();
        }

        private void SwitchShapes()
        {
            ShapeManager.CurrentShape = NextShape;            
            NextShape = Shapes.GetRandom();
        }

        /// <summary>
        /// Runs a turn in the game; that means that:
        /// 1) if currently a shape is falling, then it will move 1 cell down or 
        /// 2) if currently the grid is falling (e.g. after the last shape's last fall, there were 
        /// full lines in the grid) then it will continue falling 
        /// </summary>
        public void DoTurn()
        {
            if (!Initialized || GameOver)
                return;

            Scores.ScoreForTurn();
            if (GridManager.FallInProgress)
            {
                Scores.ScoreForLineFall();
                GridManager.Fall();
            }
            else
            {
                ShapeManager.Fall();
            }
        }

        /// <summary>
        /// Rotates the current shape, if possible. 
        /// </summary>
        public void Rotate()
        {
            if (GameOver || GridManager.FallInProgress)
                return;

            ShapeManager.Rotate();
        }

        /// <summary>
        /// Moves the current shape, if possible.
        /// </summary>
        /// <param name="x">x component of movement vector</param>
        /// <param name="y">y component of movement vector</param>
        public void Move(int x, int y)
        {
            if (GameOver || GridManager.FallInProgress)
                return;

            ShapeManager.Move(new Vector(x, y));
        }
    }
}
