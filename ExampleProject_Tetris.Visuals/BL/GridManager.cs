﻿using System;
using System.Windows;
using ExampleProject_Tetris.Visuals.Model;

namespace ExampleProject_Tetris.Visuals.BL
{
    /// <summary>
    /// Handles grid functions
    /// </summary>
    class GridManager
    {
        /// <summary>
        /// Create a gridmanager.
        /// </summary>
        /// <param name="grid">The grid to handle.</param>
        public GridManager(Grid grid)
        {
            Grid = grid;
        }

        /// <summary>
        /// The grid to handle.
        /// </summary>
        public Grid Grid { get; set; }

        /// <summary>
        /// Shows whether the grid is currently falling (e.g. after the last shape's 
        /// last fall, there were full lines in the grid)
        /// </summary>
        public bool FallInProgress { get; set; }

        /// <summary>
        /// Happens when a shape has been merged and there are full lines in the grid.
        /// </summary>
        public event EventHandler<MergeEventArgs> GridFallStarted;

        /// <summary>
        /// Happens when a shape has recently been merged and there are no more full
        /// lines in the grid. 
        /// </summary>
        public event EventHandler GridFallStopped;

        /// <summary>
        /// Returns true if all the cells that are required to contain the given points 
        /// are free. Does not check points that are above the grid (y<0)
        /// </summary>
        /// <param name="points">Points to check.</param>
        /// <returns>True if the points can be stored.</returns>
        public bool CanContain(Point[] points)
        {
            foreach (Point point in points)
            {
                if (point.Y < 0) // skipping points that are not yet in the grid  
                    continue;    // (shapes may partly be outside of the grid when appearing)

                if (!IsCellFree(point))
                {
                    return false;
                }
            }

            return true;
        }

        private bool IsCellFree(Point point)
        {
            int x = (int)point.X;
            int y = (int)point.Y;

            return IsCellFree(x, y);
        }

        private bool IsCellFree(int x, int y)
        {
            return
               x >= 0 && x < Grid.Width && y >= 0 && y < Grid.Height && // inside grid
               !Grid.Cells[x, y];                                       // and free
        }

        private bool IsLineFull(int line)
        {
            for (int x = 0; x < Grid.Width; x++)
            {
                if (Grid.Cells[x, line] == false)
                {
                    return false;
                }
            }

            return true;
        }

        private void PushLineDown(int line)
        {
            for (int x = 0; x < Grid.Width; x++)
            {
                Grid.Cells[x, line + 1] = Grid.Cells[x, line];
            }
        }

        /// <summary>
        /// Merges the shape into the grid. 
        /// </summary>
        /// <param name="shape">Shape to merge.</param>
        /// <returns>The count of the full lines created by this merge.</returns>
        public int MergeShape(Shape shape)
        {
            DoMerge(shape);
            int fullLines = CountFullLines();
            if (fullLines > 0)
            {
                FallInProgress = true;
                if (GridFallStarted != null)
                {
                    GridFallStarted(this, new MergeEventArgs(fullLines));
                }
            }

            return fullLines;
        }

        /// <summary>
        /// Clears an empty line and all the lines above that one will fall 1 line down. 
        /// </summary>
        public void Fall()
        {
            if (!FallInProgress)
                return;

            int i = Grid.Height - 1;
            while (i >= 0 && !IsLineFull(i))--i; // finds the bottommost full line
            --i; // after the bottommost full line
            while (i >= 0) // pushes each line down
            {
                PushLineDown(i);
                --i;
            }

            FallInProgress = CountFullLines() > 0;
            if (!FallInProgress && GridFallStopped != null)
            {
                GridFallStopped(this, EventArgs.Empty);
            }
        }
        
        private int CountFullLines()
        {
            int fullLines = 0;
            for (int i = Grid.Height - 1; i >= 0; i--)
            {
                if (IsLineFull(i))
                    fullLines++;
            }

            return fullLines;
        }

        private void DoMerge(Shape shape)
        {
            foreach (Point point in shape.Parts)
            {
                int pointX = (int)point.X;
                int pointY = (int)point.Y;

                // in the end it is possible that the current shape is still partially out of 
                // the grid when it gets merged. 
                if (pointX >= 0 && pointX < Grid.Width && pointY >= 0 && pointY < Grid.Height)
                { 
                    Grid.Cells[pointX, pointY] = true;
                }
            }
        }
    }
}
