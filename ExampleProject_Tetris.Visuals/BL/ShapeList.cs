﻿using System;

using ExampleProject_Tetris.Visuals.Model;

namespace ExampleProject_Tetris.Visuals.BL
{
    /// <summary>
    /// Database of the shapes that can appear in the game. 
    /// </summary>
    class ShapeList
    {
        private static Random random = new Random();

        private Shape[] possibleShapes;

        /// <summary>
        /// Reads the possible shapes from a resource file.
        /// </summary>
        public ShapeList()
        {
            possibleShapes = ReadShapesFromResourceFile();
        }

        /// <summary>
        /// Returns a copy of a random shape. 
        /// </summary>
        /// <returns>A shape that is copied from a random database shape 
        /// (e.g. points can be freely modified)</returns>
        public Shape GetRandom()
        {
            return possibleShapes[random.Next(possibleShapes.Length)].Copy();
        }

        private Shape[] ReadShapesFromResourceFile()
        {
            ShapeResourceReader reader = new ShapeResourceReader();
            string shapeFileContents = reader.ReadAll();

            // splitting on TWO newline chars - because there is an empty newline 
            // between every shape  
            string[] shapeStrings =
                shapeFileContents.Split(
                    new string[] { Environment.NewLine + Environment.NewLine },
                    StringSplitOptions.RemoveEmptyEntries);

            Shape[] possibleShapes = new Shape[shapeStrings.Length];
            for (int i = 0; i < shapeStrings.Length; i++)
            {
                possibleShapes[i] = Shape.Parse(shapeStrings[i]);
            }

            return possibleShapes;
        }
    }
}
