﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleProject_Tetris.Visuals.BL
{
    /// <summary>
    /// Contains all the score relevant functionality. 
    /// </summary>
    class Scores
    {
        private const int scoreForLineFall = 100;
        private const int scoreForTurn = 1;

        /// <summary>
        /// Scores of the player. 
        /// </summary>
        public int Score { get; private set; }

        public int MergeMultiplier { get; private set; }

        public Scores()
        {
            MergeMultiplier = 1;
        }

        /// <summary>
        /// Gives scores for a finished turn.
        /// </summary>
        public void ScoreForTurn()
        {
            Score += scoreForTurn;
        }

        /// <summary>
        /// Gives scores for a finished line fall.
        /// </summary>
        public void ScoreForLineFall()
        {
            Score += scoreForLineFall * MergeMultiplier;
        }

        /// <summary>
        /// Sets a multiplier that will affect the scores of the player 
        /// when full lines are deleted.
        /// </summary>
        /// <param name="multiplier">The multiplier to use.</param>
        public void SetMultiplierForMerge(int multiplier)
        {
            MergeMultiplier = multiplier;
        }

        /// <summary>
        /// Resets the full lines score multiplier to 1. 
        /// </summary>
        public void ResetMultiplierForMerge()
        {
            MergeMultiplier = 1;
        }
    }
}
