﻿using System.IO;
using System.Reflection;

namespace ExampleProject_Tetris.Visuals.BL
{
    /// <summary>
    /// Reads the shape resource file.  
    /// more info: http://stackoverflow.com/questions/3314140/how-to-read-embedded-resource-text-file
    /// </summary>
    class ShapeResourceReader
    {
        private const string shapeResourceFile = "ExampleProject_Tetris.Visuals.Model.shapes.txt";

        public string ReadAll()
        {
            var assembly = Assembly.GetExecutingAssembly();
            return ReadResource(assembly, shapeResourceFile);
        }

        private string ReadResource(Assembly assembly, string resourceName)
        {
            string result = string.Empty;

            using (StreamReader reader = 
                new StreamReader(assembly.GetManifestResourceStream(resourceName)))
            {
                result = reader.ReadToEnd();
            }

            return result;
        }
    }
}
