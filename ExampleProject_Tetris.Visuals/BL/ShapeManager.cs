﻿using System;
using System.Windows;

using ExampleProject_Tetris.Visuals.Model;

namespace ExampleProject_Tetris.Visuals.BL
{
    /// <summary>
    /// Handles functions of the current shape.
    /// </summary>
    class ShapeManager
    {
        /// <summary>
        /// Will fire if the current shape cannot be set, e.g. because it could not move
        /// to the starting point. This will mean the end of the game. 
        /// </summary>
        public event EventHandler CantSwitchToShape;

        /// <summary>
        /// Fires if the current shape was merged to the grid (e.g. finished falling). 
        /// </summary>
        public event EventHandler ShapeMerged;

        private Shape currentShape;

        /// <summary>
        /// Returns the current shape. 
        /// </summary>
        public Shape CurrentShape
        {
            get { return currentShape; }
            set
            {
                currentShape = value;
                if (currentShape == null)
                    return;

                bool moved = Move(gridManager.Grid.StartPoint);
                if (!moved && CantSwitchToShape != null)
                {
                    CantSwitchToShape(value, EventArgs.Empty);
                }
            }
        }

        private GridManager gridManager;
        
        /// <summary>
        /// Creates a ShapeManager.
        /// </summary>
        /// <param name="grid">The manager that handles the grid.</param>
        public ShapeManager(GridManager grid)
        {
            this.gridManager = grid;
        }

        /// <summary>
        /// Causes the shape to fall down a cell.
        /// </summary>
        public void Fall()
        {
            bool moved = Move(new Vector(0, 1));
            if (!moved)
            {
                gridManager.MergeShape(CurrentShape);
                currentShape = null;

                if (ShapeMerged != null)
                {
                    ShapeMerged(currentShape, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Causes the shape to move in a given direction.
        /// </summary>
        /// <param name="direction">The direction of the movement.</param>
        /// <returns>Returns whether the movement succeeded.</returns>
        public bool Move(Vector direction)
        {
            Point[] moved = Move(CurrentShape.Parts, direction);
            if (gridManager.CanContain(moved))
            {
                CurrentShape.Parts = moved;
                CurrentShape.CenterOfRotation = CurrentShape.CenterOfRotation + direction;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Rotates the shape to the right.
        /// </summary>
        /// <returns>Returns whether the rotation was successful.</returns>
        public bool Rotate()
        {
            if (!CurrentShape.AllowsRotation)
                return false;

            Point[] rotated = Rotate(CurrentShape.Parts, CurrentShape.CenterOfRotation);
            if (gridManager.CanContain(rotated))
            {
                CurrentShape.Parts = rotated;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Calculates the result of a movement.
        /// </summary>
        /// <param name="points">Points to move.</param>
        /// <param name="direction">Direction of the movement.</param>
        /// <returns>Result of the movement.</returns>
        private static Point[] Move(Point[] points, Vector direction)
        {
            Point[] moved = new Point[points.Length];

            for (int i = 0; i < points.Length; i++)
            {
                moved[i] = points[i] + direction;
            }

            return moved;
        }

        /// <summary>
        /// Calculates the result of a rotation.
        /// </summary>
        /// <param name="points">The points to rotate.</param>
        /// <param name="centerOfRotation">Center of rotation.</param>
        /// <returns>Result of the rotation.</returns>
        public static Point[] Rotate(Point[] points, Vector centerOfRotation)
        {
            Point[] rotated = new Point[points.Length];

            for (int i = 0; i < points.Length; i++)
            {
                Point relative = points[i] - centerOfRotation;
                // this part of the rotation is explained in... http://fizika.mechatronika.hu/matek/kgo/vektelforg90.doc
                // always rotating to the right.
                rotated[i] = new Point(relative.Y, relative.X * -1) + centerOfRotation;
            }

            return rotated;
        }
    }
}
