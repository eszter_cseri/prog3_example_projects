var indexSectionsWithContent =
{
  0: "acdefghimnprswx",
  1: "agms",
  2: "ex",
  3: "acdfgimprs",
  4: "d",
  5: "acfghimnpsw",
  6: "cgs"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties",
  6: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Properties",
  6: "Events"
};

