var searchData=
[
  ['cells',['Cells',['../class_example_project___tetris_1_1_visuals_1_1_model_1_1_grid.html#a107a1c87bd3a4b5841b05cdbf53a77e0',1,'ExampleProject_Tetris::Visuals::Model::Grid']]],
  ['centerofrotation',['CenterOfRotation',['../class_example_project___tetris_1_1_visuals_1_1_model_1_1_shape.html#ac1d6296d70270a5bd6e2da12d61b1211',1,'ExampleProject_Tetris::Visuals::Model::Shape']]],
  ['currentshape',['CurrentShape',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_game.html#a8887cb33821c96cb5fdc689427b269d2',1,'ExampleProject_Tetris.Visuals.BL.Game.CurrentShape()'],['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_shape_manager.html#a43d0866a5f82cb1e50d96195b76e519b',1,'ExampleProject_Tetris.Visuals.BL.ShapeManager.CurrentShape()']]],
  ['currentshapeactive',['CurrentShapeActive',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_game.html#a8f66eb27de8f9457dcc57264ffe674dc',1,'ExampleProject_Tetris::Visuals::BL::Game']]]
];
