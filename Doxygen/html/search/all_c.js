var searchData=
[
  ['score',['Score',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_game.html#a8bf044a27ffb7ce6f12a5c17870af017',1,'ExampleProject_Tetris.Visuals.BL.Game.Score()'],['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_scores.html#aabec0a6001240c9b7742a7259dfa8e68',1,'ExampleProject_Tetris.Visuals.BL.Scores.Score()']]],
  ['scoreforlinefall',['ScoreForLineFall',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_scores.html#a5df3a9c6e57de2eabef2d986ebfbd1e9',1,'ExampleProject_Tetris::Visuals::BL::Scores']]],
  ['scoreforturn',['ScoreForTurn',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_scores.html#a766094640faa44be92fe848c71fdba3d',1,'ExampleProject_Tetris::Visuals::BL::Scores']]],
  ['scores',['Scores',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_game.html#a21512fb6fbe0e39e403aa2755e853f54',1,'ExampleProject_Tetris::Visuals::BL::Game']]],
  ['scores',['Scores',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_scores.html',1,'ExampleProject_Tetris::Visuals::BL']]],
  ['setmultiplierformerge',['SetMultiplierForMerge',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_scores.html#ad1e15024763dc243299a6848eb860ae1',1,'ExampleProject_Tetris::Visuals::BL::Scores']]],
  ['setpropertyvalue',['SetPropertyValue',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#ade0f04c0f7b18dd5b170e071d5534d38',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.SetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, object value, System.Globalization.CultureInfo culture)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#ade0f04c0f7b18dd5b170e071d5534d38',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.SetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, object value, System.Globalization.CultureInfo culture)']]],
  ['shape',['Shape',['../class_example_project___tetris_1_1_visuals_1_1_model_1_1_shape.html',1,'ExampleProject_Tetris::Visuals::Model']]],
  ['shapelist',['ShapeList',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_shape_list.html',1,'ExampleProject_Tetris::Visuals::BL']]],
  ['shapelist',['ShapeList',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_shape_list.html#ad8498969be030461e41c0594589c8419',1,'ExampleProject_Tetris::Visuals::BL::ShapeList']]],
  ['shapemanager',['ShapeManager',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_shape_manager.html',1,'ExampleProject_Tetris::Visuals::BL']]],
  ['shapemanager',['ShapeManager',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_game.html#ad47b8b30a43a66a0bd58193a4ef58f0e',1,'ExampleProject_Tetris.Visuals.BL.Game.ShapeManager()'],['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_shape_manager.html#ac8eec351799a77fc05d71599b1e53120',1,'ExampleProject_Tetris.Visuals.BL.ShapeManager.ShapeManager()']]],
  ['shapemerged',['ShapeMerged',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_shape_manager.html#ac48ac92605a41a0679fc6382cfe51b2c',1,'ExampleProject_Tetris::Visuals::BL::ShapeManager']]],
  ['shaperesourcereader',['ShapeResourceReader',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_shape_resource_reader.html',1,'ExampleProject_Tetris::Visuals::BL']]],
  ['shapes',['Shapes',['../class_example_project___tetris_1_1_visuals_1_1_b_l_1_1_game.html#a003f80528adabba198f3a3691ec99707',1,'ExampleProject_Tetris::Visuals::BL::Game']]],
  ['startpoint',['StartPoint',['../class_example_project___tetris_1_1_visuals_1_1_model_1_1_grid.html#a0711ced714dfe1724395d213902fd326',1,'ExampleProject_Tetris::Visuals::Model::Grid']]]
];
